## 通过docker镜像搭建LNMP

##### 克隆代码：
> git clone https://git.oschina.net/kingofhua/lnmp-docker.git lnmp

##### 创建nginx文件夹,用于存放后面的相关东西 :
> mkdir -p ~/lnmp/nginx/logs

##### 创建php-fpm存放文件夹,用于存放后面的相关东西 :
> mkdir -p ~/lnmp/php-fpm/logs ~/lnmp/php-fpm/conf

##### 创建目录mysql,用于存放后面的相关东西:
> mkdir -p ~/lnmp/mysql/data ~/lnmp/mysql/logs ~/lnmp/mysql/conf

#### 使用php7.0镜像来部署php

##### 拉取官方的镜像
> docker pull php:7.0-fpm

##### 运行容器
> docker run -p 9000:9000 --name php-fpm -v ~/lnmp/nginx/www:/www -v ~/lnmp/php-fpm/conf:/usr/local/etc/php -v ~/lnmp/php-fpm/logs:/phplogs   -d php:7.0-fpm

##### 命令说明:
- -p 9000:9000 :将容器的9000端口映射到主机的9000端口
- -- --name myphp-fpm :将容器命名为myphp-fpm
- -v ~/nginx/www:/www :将主机中项目的目录www挂载到容器的/www
- -v ~/lnmp/php-fpm/conf:/usr/local/etc/php ：将主机中当前目录下的conf目录挂载到容器的/usr/local/etc/php
- -v ~/lnmp/php-fpm/logs:/phplogs ：将主机中当前目录下的logs目录挂载到容器的/phplogs

##### 查询容器IP
> docker inspect php-fpm |grep '"IPAddress"'

#### 使用nginx镜像来部署nginx

##### 拉取官方的镜像
> docker pull hub.c.163.com/library/nginx:latest

##### 修改nginx配置文件
> vim ~/lnmp/nginx/conf/default.conf

##### nginx配置文件的fastcgi_pass应该配置为myphp-fpm容器的IP
> fastcgi_pass  172.17.0.3:9000;

##### 运行容器
> docker run -p 80:80 --name nginx -v ~/lnmp/nginx/www:/www -v ~/lnmp/nginx/conf/nginx.conf:/etc/nginx/nginx.conf -v ~/lnmp/nginx/conf/default.conf:/etc/nginx/conf.d/default.conf -v ~/lnmp/nginx/logs:/var/log/nginx -d hub.c.163.com/library/nginx

##### 命令说明:
- -p 80:80：将容器的80端口映射到主机的80端口
- -- --name nginx：将容器命名为nginx
- -v ~/lnmp/nginx/www:/www：将主机中当前目录下的www挂载到容器的/www
- -v ~/lnmp/nginx/conf/nginx.conf:/etc/nginx/nginx.conf：将主机中当前目录下的nginx.conf挂载到容器的/etc/nginx/nginx.conf
- -v ~/lnmp/nginx/conf/default.conf:/etc/nginx/conf.d/default.conf ：将主机中当前目录下的域名配置挂载到容器的内部
- -v ~/lnmp/nginx/logs:/wwwlogs：将主机中当前目录下的logs挂载到容器的/wwwlogs



#### 使用mysql5.7镜像来部署mysql

##### 拉取官方的镜像,标签为5.7
> docker pull mysql:5.7

##### 运行容器
> docker run -p 3306:3306 --name mysql -v ~/lnmp/mysql/logs:/var/run/mysqld -v ~/lnmp/mysql/data:/mysql_data -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7

##### 命令说明:
- -p 80:80：将容器的80端口映射到主机的80端口
- --name mysql：将容器命名为mysql 
- -v \~/lnmp/mysql/logs:/var/run/mysqld：将主机中~/lnmp/mysql/logs挂载到容器的/logs
- -v \~/lnmp/mysql/data://mysql_data：将主机中~/lnmp/mysql/data挂载到容器的//mysql_data
- -e MYSQL_ROOT_PASSWORD=123456：初始化root用户的密码

## 关于本文

本文是通过菜鸟教程中的文章配置，按照自己的操作修改了一下。

## 感谢
- 菜鸟教程：http://www.runoob.com